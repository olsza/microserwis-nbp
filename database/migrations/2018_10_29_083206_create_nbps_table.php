<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNbpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nbps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',3);
            $table->date('date')->default( date("Y-m-d") );
            $table->decimal('rate',10,7);
            $table->char('groupTable',1)->default('A');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nbps');
    }
}
