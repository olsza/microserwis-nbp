<?php

use Illuminate\Http\Request;
use App\Http\Controllers\NbpController as NBP;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/avg/{code}',function ($code) {
	$nbp = new NBP();
	return $nbp->outAPI( $nbp->getCurrencyAvg( $code ) );
});
Route::get('/get/{code}/actual',function ($code) {
	$nbp = new NBP();
	return $nbp->outAPI( $nbp->getCurrencyActual( $code ) );
});
Route::get('/get/{code}/{date?}',function ($code,$date = NULL) {
	$nbp = new NBP();
	return $nbp->outAPI( $nbp->getCurrency( $code ,$date ) );
});
Route::get('/setDB/{date?}/{table?}',function ($date = NULL,$table = NULL) {
	$nbp = new NBP();
	$nbp->setUpdateDb( $date , $table);
	return 'OK updated';
});
Route::get('/list/{code?}',function ($code = NULL) {
	$nbp = new NBP();
	return $nbp->outAPI( $nbp->getListCurrency( $code ) );
});