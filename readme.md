**MicroSerwis NBP**

serwis udostępnia:

* listę dostępnych kody walut

	adres get: /api/list
	
	
* listę dat aktulizowanego kodu

	adres get: /api/list/{code} np. /api/list/usd
	
	
* średni kurs z danego dnia

	adres get: /api/get/{code}/{date} np. /api/get/USD/2018-10-29


* ostatni aktualny / najnowszy  średni kurs z danej waluty

	adres get: /api/get/{code}/actual np. /api/get/USD/actual
	
	
* średni kursu dla wybranej waluty, obliczony na podstawie wszystkich pobranych wcześniej (od początku życia usługi)

	adres get: /api/avg/{code} np. /api/avg/USD


* aktulizacja bazy z NBP

	* aktulizuje aktualne kursy z NBP z Tabel A i B
	
			adres get: /api/setDB	 np. /api/setDB
	


	* aktulizuje kursy z NBP z Tabel A i B z konkretnego dnia
	
			adres get: /api/setDB{date}	 np. /api/setDB/2018-10-15



	* aktulizuje aktualne kursy z NBP z Tabel B z konkretnego dnia
	
			adres get: /api/setDB{date}/{table}	 np. /api/setDB/2018-10-10/B

	Aktulizację BD z NBP mozna także zrobić za pomocą consoli, poleceniem:

			php artisan nbp:update z opcjonalnymi parametrmi jak wyżej tj. php artisan nbp:update {data?} {typTabeli?} np. "php artisan nbp:update 2018-10-12" lub "php artisan nbp:update 2018-10-14 A"
			