<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nbp;

class NbpController extends Controller
{
	// public function __construct(){
	// 	// parent::__construct();
	// }

	protected function update( $table = 'A', $date = NULL )
	{
		if(!$date)
		{
			$date = 'today';
		}
		$url = "http://api.nbp.pl/api/exchangerates/tables/" . $table . "/" . $date ."/";

		$url .= '?format=json';
 		$resHandle = @file_get_contents($url);

		return json_decode( $resHandle, true );
		// return $resHandle;
	}

	public function setUpdateDb( $date = NULL, $tables = NULL )
	{
		if( !$tables )
		{
			$tables = array('a','b');
		}

		if( !is_array($tables) )
		{
			$tables = array( $tables );
		}

		foreach ($tables as $table) {
			
			$out = $this->update( $table, $date);
			$out = $out[0] ?? NULL;

			$kursy = $out['rates'] ?? array();
			$data = $out['effectiveDate'] ?? NULL;
			foreach ($kursy as $dane) {
				$orm = [
					'code' => $dane['code'],
					'rate' => $dane['mid'],
					'date' => $data,
					'groupTable' => strtolower($table),
				];
				Nbp::updateOrCreate( $orm, $orm );
			}
		}
	}

	public function getCurrencyInfo( $code = NULL, $date = NULL )
	{
		if(!$code)
		{
			return false;
		}
		if( is_null($date) )
		{
			$date = date("Y-m-d");
		}
		$code = strtoupper($code);
		if( $date == 'actual')
		{
			return Nbp::whereCode($code)->orderBy('date','DESC')->first();
		}

		return Nbp::whereCode($code)->where('date',$date)->first();

	}

	public function getCurrency( $code = NULL, $date = NULL )
	{
		return $this->getCurrencyInfo( $code, $date )->rate ?? FALSE;
	}

	public function getCurrencyActual( $code = NULL)
	{
		return $this->getCurrencyInfo($code,'actual');
	}

	public function getCurrencyAvg( $code = NULL)
	{
		if(!$code)
		{
			return false;
		}
		$code = strtoupper($code);
		return Nbp::whereCode($code)->avg('rate');

	}

	public function getListCurrency($code = NULL)
	{
		if($code)
		{
			$out = Nbp::selectRaw('date,rate')->whereCode($code)->groupBy('date','rate')->get();
			if(count($out))
			{
				return $out;
			}
		 return NULL;
		}
		return Nbp::selectRaw('code')->groupBy('code')->get();
	}

	public function outAPI($data = NULL)
	{
		if($data === false){
			return response()->shovel( )->withError('Brak Danych',400);
			
		}
		if(!$data){
			return response()->shovel( $data )->withError('Wystąpił Błąd',500);
			
		}
		return response()->shovel( $data );
	}
}
