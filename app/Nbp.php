<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nbp extends Model
{
	protected $fillable = [
		'rate', 'code', 'date', 'groupTable',
	];
}
