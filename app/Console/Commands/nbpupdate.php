<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\NbpController;

class nbpupdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nbp:update {date?} {table?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Aktulizacja kursow walut z NBP';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $c = new NbpController;
        $result = $c->setUpdateDb( $this->argument('date'), $this->argument('table') );
    }
}
